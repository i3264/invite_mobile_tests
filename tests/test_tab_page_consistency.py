from framework.wrapper import Base


def test_tab_page_consistency():
    wrapper = Base()

    # step 1. Find all navbar tabs
    register_nav_btn = wrapper.wait_element(wrapper.general.register_nav_btn)
    rooms_nav_btn = wrapper.find_element(wrapper.general.rooms_nav_btn)
    info_nav_btn = wrapper.find_element(wrapper.general.info_nav_btn)

    elements_to_click = {
        register_nav_btn: [
            wrapper.register.register_btn
        ],
        rooms_nav_btn: [
            wrapper.rooms.page_announcement
        ],
        info_nav_btn: [
            wrapper.info.sharing_info_title,
            wrapper.info.creator_one
        ]
    }
    # step 2. Click on every navbar tab and check its consistency
    for page, elements in elements_to_click.items():
        try:
            page.click()
        except Exception as e:
            raise Exception(f'Unable to click on {page} {e}')

        for elem in elements:
            wrapper.wait_element(elem)
