from framework.wrapper import Base


def test_send_munich_registration():
    wrapper = Base()

    # step 1. Select the "Munich" office location
    default_city = wrapper.wait_element(wrapper.register.regensburg)
    default_city.click()
    munich = wrapper.wait_element(wrapper.register.munich)
    munich.click()

    # step 2. Select the date for upcoming visit
    tomorrow = wrapper.wait_element(wrapper.register.tomorrow)
    tomorrow.click()
    register_btn = wrapper.find_element(wrapper.register.register_btn)
    register_btn.click()
    wrapper.find_element(wrapper.register.register_btn)

    # step 3. Check the "Send registration email" was successful
    wrapper.find_element(wrapper.register.successfully_registered_message)
