class RegisterLocators:

    @property
    def register_btn(self):
        return ('accessibility id', 'Send registration email', 'Send registration email button')

    @property
    def regensburg(self):
        return ('accessibility id', 'Regensburg', 'Regensburg city to send data')

    @property
    def munich(self):
        return ('accessibility id', 'München', 'München city to send data')

    @property
    def tomorrow(self):
        return ('accessibility id', 'Tomorrow', '"Tomorrow" as day to send data')

    @property
    def successfully_registered_message(self):
        return ('accessibility id', 'Successfully registered your office visit for tomorrow', 'Successfully registered message')
