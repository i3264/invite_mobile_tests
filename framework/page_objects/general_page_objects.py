class GeneralLocators:

    @property
    def register_nav_btn(self):
        return ('accessibility', 'Register visit\nTab 1 of 3', 'Register registration email button')

    @property
    def rooms_nav_btn(self):
        return ('accessibility id', 'Rooms\nTab 2 of 3', 'Rooms button in bottom nav bar')

    @property
    def info_nav_btn(self):
        return ('accessibility id', 'Info\nTab 3 of 3', 'Info button in bottom nav bar')
