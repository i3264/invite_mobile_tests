class InfoLocators:

    @property
    def creator_one(self):
        return ('accessibility id', 'Creator 1', 'Creator name')

    @property
    def sharing_info_title(self):
        return ('accessibility id', 'Share this app with others', 'Sharing info title')
