from time import sleep

from appium.webdriver.common.touch_action import TouchAction

from framework.appium_driver_manager.appium_driver_manager import AppiumDriverManager
from framework.page_objects.general_page_objects import GeneralLocators
from framework.page_objects.info_page_objects import InfoLocators
from framework.page_objects.register_page_objects import RegisterLocators
from framework.page_objects.rooms_page_objects import RoomsLocators


class Base:
    def __init__(self):
        self.manager = AppiumDriverManager()
        self.driver = self.manager.driver()
        self.app = self.driver.desired_capabilities.get('app')
        self.device_name = self.driver.desired_capabilities.get('deviceName')
        self.general = GeneralLocators()
        self.register = RegisterLocators()
        self.rooms = RoomsLocators()
        self.info = InfoLocators()

    def find_element(self, locator):
        method = locator[0]
        locator_id = locator[1]
        element = None
                try:
            element = self.driver.find_element(by=method, value=locator_id)
        except Exception as e:
            return False
            print(f'Element {locator[2]} not found on the screen {e}')
        return element

    def wait_element(self, locator):
        waiting = 0
        element = None
        while not element and waiting < 5:
            element = self.find_element(locator)
            if element:
                return element
            waiting += 1
            sleep(1)
