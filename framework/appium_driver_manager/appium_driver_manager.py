from appium import webdriver
app_name = 'com.example.office_app'
android_capabilities = {
    'automationName': 'UiAutomator2',
    'platformName': 'Android',
    'appPackage': app_name,
    'appActivity': f'{app_name}.MainActivity'
}


class AppiumDriverManager():
    def __init__(self):
        self.appium = webdriver.Remote('http://0.0.0.0:4723/wd/hub', android_capabilities)

    def driver(self):
        if self.appium:
            return self.appium
        raise ReferenceError('Please initialize appium: AppiumDriverManager().init()')

    def quit(self):
        if self.appium:
            self.appium.quit()
